.global rgb_to_gray

.section .data
.set R, 77
.set G, 151
.set B, 28
// 8 pixels will be processed at once, below - prepared constants for R/G/B value of each pixel:
coefficients: .byte R, R, R, R, R, R, R, R, G, G, G, G, G, G, G, G, B, B, B, B, B, B, B, B

.section .text
.arm
.fpu neon

/*
rgb_to_gray arguments:
r0 - void *dst
r1 - void *src
r2 - size_t pixels_number
*/
rgb_to_gray:
    ldr         r3, =coefficients
    vld1.8      {d0-d2}, [r3]   // populate registers d0, d1, d2 with constants R/G/B
    lsrs        r3, r2, #3      // r3 = size/8, set flags
    and         r2, r2, #7      // r2 = size%8
    beq         remaining_bytes // if r3 == 0: nothing to do in vector_loop
vector_loop:
    vld3.8      {d3-d5}, [r1]!  // load 8 pixels with de-interleaving (d3 - all 'red', d4 - all 'green', d5 - all 'blue'), add number of loaded bytes to r1
    vmull.u8    q3, d0, d3      // multiply red component (Long - q3 is used for result, because q0, q1, q2 would override (d0,d1), (d2,d3), (d4, d5))
    vmlal.u8    q3, d1, d4      // multiply green componenet and accumulate (add to red)
    vmlal.u8    q3, d2, d5      // as above, blue component
    vshrn.u16   d6, q3, #8      // shr 8 (divide by 256), Narrow (change back to 8-bit numbers)
    vst1.8      {d6}, [r0]!     // store, add number of stored bytes to r0
    subs        r3, r3, #1      // decrement loop counter and set flags
    bne         vector_loop
remaining_bytes:
    cmp         r2, #0
    bxeq        lr              // no remaining bytes -> end
    push        {r4-r6, r8}     // remaining bytes will be processed without NEON, additional registers will be needed
    mov         r4, #R
    mov         r5, #G
    mov         r6, #B
remaining_bytes_loop:
    ldrb        r8, [r1], #1    // load one byte (red value) and increment r1
    mul         r3, r8, r4      // multiply red component and save it in r3
    ldrb        r8, [r1], #1    // load one byte (green value) and increment r1
    mla         r3, r8, r5, r3  // multiply green component and accumulate (add to r3)
    ldrb        r8, [r1], #1    // load one byte (blue value) and increment r1
    mla         r3, r8, r6, r3  // multiply blue component and accumulate (add to r3)
    lsr         r3, r3, #8      // divide by 256 - normalize
    strb        r3, [r0], #1    // save result (one gray pixel) and increment r0
    subs        r2, #1
    bne         remaining_bytes_loop
end:
    pop         {r4-r6, r8}
    bx          lr
