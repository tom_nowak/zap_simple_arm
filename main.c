#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stb_image.h"
#include "stb_image_write.h"

typedef struct
{
    int width, height, bytes_per_pixel;
    unsigned char *data;
} Image;

void print_help_and_exit(const char *program_name);
int check_execution_mode(int argc, char **argv);
void load(const char *filename, Image *img);
void rgb_to_gray(void *dst, void *src, size_t pixels_number); // asm function, src can be the same as dst (to override data in input image)
void rgb_to_gray_c(unsigned char *dst, unsigned char *src, size_t pixels_number); // C function, producing the same results as asm (to compare performane)
void save_and_free_buffer(const char *filename, Image *img);

int main(int argc, char **argv)
{
    Image img;
    int mode = check_execution_mode(argc, argv);
    load(argv[2], &img);
    if(mode == 0)
        rgb_to_gray_c(img.data, img.data, img.width * img.height);
    else
        rgb_to_gray(img.data, img.data, img.width * img.height);
    printf("converted to gray\n");
    img.bytes_per_pixel = 1;
    save_and_free_buffer(argv[3], &img);
    return 0;
}

void print_help_and_exit(const char *program_name)
{
    printf("Usage: %s [execution mode] [path to input image] [path to output image]\n"
           "[execution mode] can be:\n"
           "-c : use C function\n"
           "-a : use assembly function with vectorized operations\n", program_name);
    exit(1);
}

int check_execution_mode(int argc, char **argv)
{
    int mode;
    if(argc != 4)
        print_help_and_exit(argv[0]);
    if(strcmp(argv[1], "-c") == 0)
        mode = 0;
    else if(strcmp(argv[1], "-a") == 0)
        mode = 1;
    else
        print_help_and_exit(argv[0]);
    return mode;
}

void load(const char *filename, Image *img)
{
    img->data = stbi_load(filename, &img->width, &img->height, &img->bytes_per_pixel, 3); // 3 - force 3 channels (RBG)
    if(!img->data)
    {
        printf("failed to load image: %s\nerror message: %s\n", filename, stbi_failure_reason());
        exit(-1);
    }
    printf("loaded image with size %ix%i\n", img->width, img->height);
}

void save_and_free_buffer(const char *filename, Image *img)
{
    int length = strlen(filename);
    if(length >= 4)
    {
        if(strcmp(filename + length - 4, ".png") != 0) // only png is supported for output image - warn user if there is no .png in filename
            printf("image will be saved in png format\n");
    }
    if(stbi_write_png(filename, img->width, img->height, img->bytes_per_pixel, img->data, img->width * img->bytes_per_pixel) == 0)
    {
        printf("failed to write output image to file: %s\n", filename);
        stbi_image_free(img->data);
        exit(-3);
    }
    printf("saved\n");
    stbi_image_free(img->data);
}

void rgb_to_gray_c(unsigned char *dst, unsigned char *src, size_t pixels_number)
{
    int gray;
    size_t i;
    for(i = 0; i < pixels_number; ++i)
    {
        gray = 77 * src[0] + 151 * src[1] + 28 * src[2];
        gray /= 256;
        *dst = (unsigned char)(gray);
        ++dst;
        src += 3;
    }
}

