## RGB -> grayscale converter written in ARM assembly, using NEON vector unit  
  
#### Used tools  
Hardware:  
Raspberry Pi 2B+ (processor model ARMv7 Processor rev 5 (v7l))  
  
Software:  
[Raspbian OS](https://www.raspberrypi.org/downloads/raspbian/)  
GNU make  
gcc (GNU compiler for C)  
as (GNU assembler for ARM)  
[stb\_image and stb\_image\_write mini-libraries](https://github.com/nothings/stb)  
  
#### Building  
Note that this program was written for particular hardware and is not meant to be portable.  
Feel free to modify Makefile before running make.  
```
mkdir build  
make  
```
  
#### Running  
(path to convert\_to\_gray program) (execution mode) (path to input image) (path to output image)  
(execution mode) can be:  
-c : use C function  
-a : use assembly function with vectorized operations  
  
C and assembly functions should produce the same results (confirmed in my tests). Moreover, you can compare execution times in both cases. My tests have shown that assembly version is a bit faster, but in practice it is negligible, because saving output image takes the most of program execution time. This project was written purely for educational purposes.
