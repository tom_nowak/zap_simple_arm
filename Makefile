CC = gcc
AS = as
MY_CFLAGS = -Wall -Wextra -pedantic -O3 -march=native
LIB_CFLAGS = -O3 -march=native # ignore warnings in library code - I am not going to change it anyway
BIN = build/
SRC = ./

all: $(BIN)main.o $(BIN)stb_image.o $(BIN)asm.o
	$(CC) $(MY_CFLAGS) $(BIN)main.o $(BIN)stb_image.o $(BIN)asm.o -o $(BIN)convert_to_gray -lm

$(BIN)main.o: $(SRC)main.c
	$(CC) $(MY_CFLAGS) -c $(SRC)main.c -o $(BIN)main.o

$(BIN)stb_image.o: $(SRC)stb_image.c $(SRC)stb_image.h $(SRC)stb_image_write.h
	$(CC) $(LIB_CFLAGS) -c $(SRC)stb_image.c -o $(BIN)stb_image.o

$(BIN)asm.o: $(SRC)asm.s
	$(AS) $(SRC)asm.s -o $(BIN)asm.o

clean:
	rm $(BIN)*.o
